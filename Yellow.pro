#-------------------------------------------------
#
# Project created by QtCreator 2016-11-10T23:53:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 console precompile_header
QMAKE_CXXFLAGS += -std=c++11

TARGET = Yellow
TEMPLATE = app

PRECOMPILED_HEADER = stdafx.h

SOURCES += main.cpp\
        mainwindow.cpp \
    EthHeader.cpp \
    IPHeader.cpp \
    Packet.cpp \
    PCAPRecord.cpp \
    PCAPRecorder.cpp \
    Sniffer.cpp \
    TCPHeader.cpp \
    UDPHeader.cpp \
    SnifferThread.cpp

HEADERS  += mainwindow.h \
    EthHeader.h \
    IPHeader.h \
    ITLHeader.h \
    Packet.h \
    Payload.hpp \
    PCAPHeader.h \
    PCAPRecord.h \
    PCAPRecorder.h \
    Sniffer.h \
    TCPHeader.h \
    UDPHeader.h \
    SnifferThread.h

FORMS    += mainwindow.ui

RESOURCES += \
    icons.qrc


