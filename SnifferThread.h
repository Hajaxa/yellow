#ifndef SNIFFERTHREAD_H
#define SNIFFERTHREAD_H

#include <QThread>
#include <QObject>
#include "Sniffer.h"
#include "Packet.h"

class SnifferThread : public QThread
{
    Q_OBJECT
public:
    SnifferThread(Yellow::Sniffer &snif);
    ~SnifferThread();

    void                            run();

signals:
    void                            newPacket(std::shared_ptr<Yellow::Packet> pack);

private:
    Yellow::Sniffer                 &sniffer;
};

#endif // SNIFFERTHREAD_H
