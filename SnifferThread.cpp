#include "stdafx.h"
#include "SnifferThread.h"

SnifferThread::SnifferThread(Yellow::Sniffer &snif)
    : sniffer(snif)
{
}

SnifferThread::~SnifferThread()
{

}

void            SnifferThread::run()
{
    std::cout << "run initiated\n";

    std::shared_ptr<Yellow::Packet> pack;
    while ((pack = this->sniffer.getPacket()) != nullptr)
        emit this->newPacket(pack);
}
