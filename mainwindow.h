#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <vector>
#include <QObject>
#include <QMainWindow>
#include <QTreeWidget>
#include <QTableWidget>
#include <QDebug>
#include <QThread>
#include <QFileDialog>
#include <QRegularExpression>
#include <QTimer>
#include "PCAPRecorder.h"
#include "Sniffer.h"
#include "Packet.h"
#include "TCPHeader.h"
#include "UDPHeader.h"
#include "SnifferThread.h"

Q_DECLARE_METATYPE(std::shared_ptr<Yellow::Packet>)


typedef struct                  s_packetInfo    // Struct containing all the informations related to the Packet
{
    std::string                 no;
    std::string                 time;
    std::string                 source;
    std::string                 destination;
    std::string                 protocol;
    std::string                 length;
    std::string                 info;
}                               t_packetInfo;

typedef struct                  s_packetTree    // Struct containing all strings with its children
{
    std::string                 main_string;
    std::vector<s_packetTree>   vecChildren;
}                               t_packetTree;

typedef struct                  s_packetData    // Struct with 16 std::string which contains 2 char each, and 16 char for the ASCII characters
{
    std::string                 hexa[16];
    char                        ascii[16];
}                               t_packetData;

typedef struct                  s_packet
{
    std::string                time;            // Neccessary ? used to check the identity of the packet when clicked
    std::vector<t_packetTree>  vecPacket;      // Contains all details concerning the packet going into the Packet TreeWidget
    std::vector<t_packetData>  vecData;        // Contains all the raw data of the packet

}                               t_packet;


typedef struct                  s_QPacket       // Struct containing directly the Widgets instead of refilling them every time. Increase load speed when clicked
{
    QTreeWidget                 *packetTree;
    QTableWidget                *rawDataTable;
    std::shared_ptr<Yellow::PCAP::Record>   record;
}                               t_QPacket;

namespace   Ui {
class       MainWindow;
}

class   MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit                    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    t_packetInfo                getPacketInfo(std::shared_ptr<Yellow::PCAP::Record> record);
    std::vector<t_packetTree>   getPacketTrees(std::shared_ptr<Yellow::Packet> pack);
    std::vector<t_packetData>   getPacketData(std::shared_ptr<Yellow::Packet> pack);
    std::string                 getTime(std::shared_ptr<Yellow::PCAP::Record> currentPacket);

    t_packetTree                getEthHeader(const Yellow::EthHeader &ethHeader);
    t_packetTree                getIpHeader(const Yellow::IPHeader &ipHeader);
    t_packetTree                getTCPHeader(std::shared_ptr<Yellow::ITLHeader> itlHeader);
    t_packetTree                getUDPHeader(std::shared_ptr<Yellow::ITLHeader> itlHeader);
    std::string                 getStrFlagSet(unsigned int flag);


    void                        addPacket(t_packetInfo &packetInfo, t_packet &packet, std::shared_ptr<Yellow::PCAP::Record> record);

    void                        addPacketLine(t_packetInfo &packetInfo);            // Add line concerning the packet in the Packet QTableWidget
    void                        addPacketLine(std::vector<std::string> packetInfo);
    void                        addPacketLine(std::string &no, std::string &time, std::string &src, std::string &dest,
                                        std::string &prtcl, std::string &len, std::string &info);

    void                        pushPacket(t_packet &packet, std::shared_ptr<Yellow::PCAP::Record> record);                       // Create new t_QPacket, parse t_packet &packet, create QWidget and store in vec

    QTreeWidget                 *parsePacket(std::vector<t_packetTree> vecPacket); // Parse the tree of the packet details
    QTreeWidgetItem             *setTreeWidgetItem(t_packetTree stringTree);       // Return a correctly formed string tree

    QTableWidget                *parseData(std::vector<t_packetData> vecData);     // Parse rawData, hex / Ascii and return QTableWidget


    void                        fillPacketTree(QTreeWidget *tree);
    void                        fillDataTable(QTableWidget *table);

    void                        clearVecPacketWidget();
    void                        filterPacket(std::vector<QString> vecFilters);
    void                        fillFilteredTable(std::vector<int> vecIndex);

    void                        ppjWait(int ms);

private slots:
    void                        receiveNewPacket(std::shared_ptr<Yellow::Packet> pack);
    void                        on_packetTable_cellClicked(int row, int column);
    void                        on_dataTable_cellClicked(int row, int column);
    void                        on_actionSave_triggered();
    void                        on_actionLoad_triggered();

    void                        on_actionStart_triggered();
    void                        on_actionStop_triggered();

    void                        on_actionFilter_triggered();
    void                        on_btnApply_clicked();
    void                        on_btnCancel_clicked();

    void                        on_checkNo_stateChanged(int arg1);
    void                        on_checkTime_stateChanged(int arg1);
    void                        on_checkLength_stateChanged(int arg1);
    void                        on_checkSource_stateChanged(int arg1);
    void                        on_checkDest_stateChanged(int arg1);
    void                        on_checkProto_stateChanged(int arg1);

    void                        on_filteredTable_cellClicked(int row, int column);

private:
    Ui::MainWindow                  *ui;
    std::vector<t_QPacket*>         vecPacketWidget;
    Yellow::Sniffer                 snif;
    Yellow::PCAP::Recorder          rec;
    SnifferThread                   *snifThread;
};

#endif // MAINWINDOW_H
