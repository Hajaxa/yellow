#include <QApplication>
#include "stdafx.h"
#include "mainwindow.h"

int		main(int argc, char **argv)
{
  /*Yellow::Sniffer s;
  Yellow::PCAP::Recorder  rec;

  std::shared_ptr<Yellow::Packet> pack; // fonction recup packet
  std::ofstream ofs ("test.txt", std::ofstream::out | std::ofstream::trunc | std::ofstream::binary); // fichier sortie outputstream
  if (!ofs)
    {
      std::cout << "fail to get file" << std::endl;
      return 0;
    }

  if (!s.start())
    {
      std::cout << "Fail tu start" << std::endl;
      return 0;
    }
  while ((pack = s.getPacket()) != nullptr)
    {
      if (pack->getEthHeader().getProto() == 8 && pack->getIPHeader().getProtocol() == 6)
        {
          rec.addRecord(pack);
          ofs << rec;
          ofs.seekp(std::ios_base::beg);
          std::cout << "Source IP : " << pack->getIPHeader().getSourceIP()
                    << ", Destination IP :" << pack->getIPHeader().getDestinationIP() << std::endl;
        }
    }*/


  QApplication a(argc, argv);

  qRegisterMetaType<std::shared_ptr<Yellow::Packet>>();

  MainWindow w;
  w.show();


  /*t_packetInfo    packet;
  packet.no = new std::string("1");
  packet.time = new std::string("1.0050");
  packet.source = new std::string ("192.168.1.1");
  packet.destination = new std::string("192.168.1.2");
  packet.protocol = new std::string("UDP");
  packet.length = new std::string("55");
  packet.info = new std::string("9999 -> 8888");

  t_packetInfo    packet2;
  packet2.no = new std::string("2");
  packet2.time = new std::string("2.0000");
  packet2.source = new std::string ("192.168.1.3");
  packet2.destination = new std::string("192.168.1.4");
  packet2.protocol = new std::string("UDP");
  packet2.length = new std::string("55");
  packet2.info = new std::string("9999 -> 8888");

  t_packetData    packetData = {{"60", "e3", "60", "b4", "60", "e3", "60", "b4", "62", "e3", "60", "b4", "60", "73", "60", "24"},
                                {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'}};

  t_packetData    packetData2 = {{"60", "e3", "44", "b4", "10", "e3", "60", "b4", "ab", "e3", "60", "e3", "mz", "73", "60", "24"},
                                {'z', 'y', 'x', 'w', 'v', 'u', 't', 's', 'r', 'q', 'p', 'l', 'm', 'n', 'o', 'p'}};

  t_packetTree    packetTree;
  t_packetTree    packetTreeChild;
  packetTree.main_string = new std::string("First try");
  packetTreeChild.main_string = new std::string("child of first try");
  packetTree.vecChildren.push_back(&packetTreeChild);


  t_packetTree    packetTree2;
  t_packetTree    packetTreeChild2;
  packetTree2.main_string = new std::string("Second try");
  packetTreeChild2.main_string = new std::string("child of first second try");
  packetTree2.vecChildren.push_back(&packetTreeChild2);

  t_packet        finalpacket;
  finalpacket.vecPacket.push_back(&packetTree);
  finalpacket.vecData.push_back(&packetData);

  t_packet        finalpacket2;
  finalpacket2.vecPacket.push_back(&packetTree2);
  finalpacket2.vecData.push_back(&packetData2);

  w.addPacket(packet, finalpacket);
  w.addPacket(packet2, finalpacket2);*/
  return a.exec();
  return 0;
}
