#include "stdafx.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#define ARRAY_SIZE 16             // Size of the std::string array in the t_packetData struct

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QString                     ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    QRegularExpression          ipRegex ("^" + ipRange + "\\." + ipRange + "\\." + ipRange + "\\." + ipRange + "$");
    QRegularExpressionValidator *ipValidator = new QRegularExpressionValidator(ipRegex, this);

    this->ui->sourceLineEdit->setValidator(ipValidator);
    this->ui->destinationLineEdit->setValidator(ipValidator);
    this->ui->sourceLineEdit->setPlaceholderText("ex: 192.168.1.1");
    this->ui->destinationLineEdit->setPlaceholderText("ex: 192.168.1.1");

    this->ui->groupBox->hide();
    this->ui->filteredTable->hide();

    this->ui->dataTable->setColumnCount(ARRAY_SIZE * 2);
    this->ui->dataTable->horizontalHeader()->hide();
    this->ui->dataTable->setColumnWidth((ARRAY_SIZE / 2) - 1, 50);
    this->ui->dataTable->setColumnWidth(ARRAY_SIZE - 1, 65);
    this->ui->dataTable->setColumnWidth((ARRAY_SIZE + (ARRAY_SIZE / 2)) - 1, 50);

    this->ui->packetTable->setColumnWidth(0, 50);
    this->ui->packetTable->setColumnWidth(1, 200);

    this->ui->filteredTable->setColumnWidth(0, 50);
    this->ui->filteredTable->setColumnWidth(1, 200);

    if (!this->snif.start())
      {
        std::cout << "Fail to start sniffer. Exiting Yellow." << std::endl;
        this->close();
      }
    snifThread = new SnifferThread(snif);
    connect(snifThread, SIGNAL(newPacket(std::shared_ptr<Yellow::Packet>)), this, SLOT(receiveNewPacket(std::shared_ptr<Yellow::Packet>)));
}

MainWindow::~MainWindow()
{
    delete this->snifThread;
    delete ui;
}

void                        MainWindow::receiveNewPacket(std::shared_ptr<Yellow::Packet> pack)
{
    qDebug() << "receive new packet";
    std::shared_ptr<Yellow::PCAP::Record>   currentPacket = this->rec.addRecord(pack);
    t_packetInfo                            packetInfo = this->getPacketInfo(currentPacket);
    t_packet                                packet;

    packet.vecPacket = this->getPacketTrees(pack);
    packet.vecData = this->getPacketData(pack);

    this->addPacket(packetInfo, packet, currentPacket);
}

t_packetInfo                MainWindow::getPacketInfo(std::shared_ptr<Yellow::PCAP::Record> record)
{
    t_packetInfo            packetInfos;

    packetInfos.no = QString::number(this->ui->packetTable->rowCount()).toStdString();
    packetInfos.time = this->getTime(record);
    packetInfos.source = record->getPacket()->getIPHeader().getSourceIP();
    packetInfos.destination = record->getPacket()->getIPHeader().getDestinationIP();
    if (record->getPacket()->getIPHeader().getProtocol() == 17)
        packetInfos.protocol = "UDP";
    else
        packetInfos.protocol = "TCP";
    packetInfos.length = std::to_string(record->getPacket()->getLenght());
    packetInfos.info = "rien";

    return (packetInfos);
}

std::vector<t_packetTree>   MainWindow::getPacketTrees(std::shared_ptr<Yellow::Packet> pack)
{
    t_packetTree                ethHeader = this->getEthHeader(pack->getEthHeader());
    t_packetTree                ipHeader = this->getIpHeader(pack->getIPHeader());
    t_packetTree                tlHeader;
    std::vector<t_packetTree>   vecTrees;

    if (pack->getIPHeader().getProtocol() == 6) // == TCP
        tlHeader = this->getTCPHeader(pack->getTLHeader());
    else
        tlHeader = this->getUDPHeader(pack->getTLHeader());

    vecTrees.push_back(ethHeader);
    vecTrees.push_back(ipHeader);
    vecTrees.push_back(tlHeader);

    return (vecTrees);
}

std::vector<t_packetData>       MainWindow::getPacketData(std::shared_ptr<Yellow::Packet> pack)
{
    std::vector<t_packetData>   vecData;
    t_packetData                data;
    int                         iArray = 0;

    for (int i = 0; i < pack->getLenght(); ++i)
    {
        //std::stringstream       ss;
        unsigned char           c = *(*pack)[i];
        char                    str_hex[3];


        if (iArray == ARRAY_SIZE)                       // Store result and create new one
        {
            vecData.push_back(data);
            data = t_packetData();
            iArray = 0;
        }

        //ss << std::hex << (int)c;
        //data.hexa[iArray] = ss.str();
        sprintf(str_hex, "%.2X", (int)c);
        //str_hex[2] = 0;
        data.hexa[iArray] = str_hex;
        if (c >= 33 && c <= 126)
            data.ascii[iArray] = c;
        else
            data.ascii[iArray] = '.';
        ++iArray;
    }
    for (iArray; iArray != ARRAY_SIZE; ++iArray)       // packLength != multiplicator of ARRAY_SIZE
        data.ascii[iArray] = '\0';
    vecData.push_back(data);

    return (vecData);
}



/*
 *      GET TIME AND HEADERS
 */

std::string                     MainWindow::getTime(std::shared_ptr<Yellow::PCAP::Record> currentPacket)
{
    std::stringstream           stringstream;
    std::time_t                 tsec = currentPacket->getHeader().ts_sec;
    std::size_t                 tusec = currentPacket->getHeader().ts_usec;
    char                        buffer[26];
    struct tm                   *tm_info;

    tm_info = localtime(&tsec);
    strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", tm_info);

    stringstream << buffer << "." << tusec;
    qDebug() << "Debug: time = " << stringstream.str().c_str();
    return (stringstream.str());
}

t_packetTree                    MainWindow::getEthHeader(const Yellow::EthHeader &ethHeader)
{
    t_packetTree                eth;
    t_packetTree                tree;

    eth.main_string = "Ethernet";
    for (int i = 0; i < 2; ++i)
    {
        std::stringstream       ss;

        if (i == 0)
            ss << "Destination: ";
        else
            ss << "Source: ";
        for(int n = 0; n < ethHeader.MacLenght; ++n)
        {
            ss << std::hex << (int)ethHeader.getDest()[n];
            if (n + 1 != ethHeader.MacLenght)
                ss << ":";
        }
        tree.main_string = ss.str();
        eth.vecChildren.push_back(tree);
    }
    t_packetTree    type;
    type.main_string = std::string("Type: ") + std::to_string(ethHeader.getProto());
    eth.vecChildren.push_back(type);

    return (eth);
}

t_packetTree                MainWindow::getIpHeader(const Yellow::IPHeader &ipHeader)
{
    t_packetTree            ip;
    t_packetTree            tree;
    std::string             titles[10] = {"Source: ", "Destination: ", "Checksum: ", "Header length: ",
                                          "Total length: ", "Identification: ", "Protocol: ", "TTL: ",
                                          "Type of service: ", "Version: "};

    ip.main_string = "Internet Protocol";

    for (int i = 0; i < 10; ++i)
    {
        std::stringstream       ss;

        ss << titles[i];
        if (i == 0)
            ss << ipHeader.getSourceIP();
        else if (i == 1)
            ss << ipHeader.getDestinationIP();
        else if (i == 2)
            ss << ipHeader.getChecksum();
        else if (i == 3)
            ss << ipHeader.getHeaderLenght();
        else if (i == 4)
            ss << ipHeader.getTotalLenght();
        else if (i == 5)
            ss << ipHeader.getIndentification();
        else if (i == 6)
            ss << ipHeader.getProtocol();
        else if (i == 7)
            ss << ipHeader.getTTL();
        else if (i == 8)
            ss << ipHeader.getTypeOfService();
        else if (i == 9)
            ss << ipHeader.getVersion();
        tree.main_string = ss.str();
        ip.vecChildren.push_back(tree);
    }
    return (ip);
}

t_packetTree                MainWindow::getTCPHeader(std::shared_ptr<Yellow::ITLHeader> itlHeader)
{
    std::shared_ptr<Yellow::TCPHeader>  tcpHeader = std::dynamic_pointer_cast<Yellow::TCPHeader>(itlHeader);
    t_packetTree                        tcp;
    t_packetTree                        tree;
    std::string                         titles[9] = {"Source: ", "Destination: ", "Sequence number: ", "Aknowledge number: ",
                                          "Header length: ", "Flags ", "Window: ", "Checksum: ",
                                          "Pointer: "};
    std::string                         flag_titles[6] = {"Urgent: ", "Aknowledgment: ", "Push: ", "Reset: ",
                                                          "Synchronize: ", "Finish: "};
    tcp.main_string = "Transmission Control Protocol";

    for (int i = 0; i < 9; ++i)
    {
        std::stringstream       ss;

        ss << titles[i];
        if (i == 0)
            ss << tcpHeader->getSourcePort();
        else if (i == 1)
            ss << tcpHeader->getDestPort();
        else if (i == 2)
            ss << tcpHeader->getSequenceNumber();
        else if (i == 3)
            ss << tcpHeader->getAcknowledgeNumber();
        else if (i == 4)
            ss << tcpHeader->getHeaderLenght();
        else if (i == 5) // == FLAGS
        {
            for (int n = 0; n < 6; ++n)
            {
                t_packetTree        flag;
                std::stringstream   sub_ss;

                sub_ss << flag_titles[n];
                if (n == 0)
                    sub_ss << this->getStrFlagSet(tcpHeader->getUrgentFlag());
                else if (n == 1)
                    sub_ss << this->getStrFlagSet(tcpHeader->getAcknowledgementFlag());
                else if (n == 2)
                    sub_ss << this->getStrFlagSet(tcpHeader->getPushFlag());
                else if (n == 3)
                    sub_ss << this->getStrFlagSet(tcpHeader->getResetFlag());
                else if (n == 4)
                    sub_ss << this->getStrFlagSet(tcpHeader->getSynchroniseFlag());
                else if (n == 5)
                    sub_ss << this->getStrFlagSet(tcpHeader->getFinishFlag());
                flag.main_string = sub_ss.str();
                tree.vecChildren.push_back(flag);
            }
        }
        else if (i == 6)
            ss << tcpHeader->getWindow();
        else if (i == 7)
            ss << tcpHeader->getChecksum();
        else if (i == 8)
            ss << tcpHeader->getUrgentPointer();
        tree.main_string = ss.str();
        tcp.vecChildren.push_back(tree);
        if (i == 5)
            tree.vecChildren.clear();
    }
    return (tcp);
}

t_packetTree                MainWindow::getUDPHeader(std::shared_ptr<Yellow::ITLHeader> itlHeader)
{
    std::shared_ptr<Yellow::UDPHeader>  udpHeader = std::dynamic_pointer_cast<Yellow::UDPHeader>(itlHeader);
    t_packetTree                        udp;
    t_packetTree                        tree;
    std::string                         titles[4] = {"Source: ", "Destination: ", "Length: ", "Checksum: "};

    udp.main_string = "User Datagram Protocol";

    for (int i = 0; i < 4; ++i)
    {
        std::stringstream       ss;

        ss << titles[i];
        if (i == 0)
            ss << udpHeader->getSourcePort();
        else if (i == 1)
            ss << udpHeader->getDestPort();
        else if (i == 2)
            ss << udpHeader->getUDPLenght();
        else if (i == 3)
            ss << udpHeader->getChecksum();
        tree.main_string = ss.str();
        udp.vecChildren.push_back(tree);
    }
    return (udp);
}

std::string                 MainWindow::getStrFlagSet(unsigned int flag)
{
    if (flag == 0)
        return (std::string("Not set"));
    return (std::string("Set"));
}



/*
 *      NOUVEAU PACKET CREE
 */

void                        MainWindow::addPacket(t_packetInfo &packetInfo, t_packet &packet, std::shared_ptr<Yellow::PCAP::Record> record)
{
    qDebug() << "addPacket";
    this->addPacketLine(packetInfo);
    this->pushPacket(packet, record);
}



/*
 *          AJOUTER LIGNE DANS TABLEAU
 */

void                        MainWindow::addPacketLine(t_packetInfo &packetInfo)
{
    qDebug() << "addPacketLine";
    int                     rowCount = this->ui->packetTable->rowCount();

    this->ui->packetTable->setRowCount(rowCount + 1);
    this->ui->packetTable->setItem(rowCount, 0, new QTableWidgetItem(packetInfo.no.c_str()));
    this->ui->packetTable->setItem(rowCount, 1, new QTableWidgetItem(packetInfo.time.c_str()));
    this->ui->packetTable->setItem(rowCount, 2, new QTableWidgetItem(packetInfo.source.c_str()));
    this->ui->packetTable->setItem(rowCount, 3, new QTableWidgetItem(packetInfo.destination.c_str()));
    this->ui->packetTable->setItem(rowCount, 4, new QTableWidgetItem(packetInfo.protocol.c_str()));
    this->ui->packetTable->setItem(rowCount, 5, new QTableWidgetItem(packetInfo.length.c_str()));
    this->ui->packetTable->setItem(rowCount, 6, new QTableWidgetItem(packetInfo.info.c_str()));
}

void                        MainWindow::addPacketLine(std::vector<std::string> packetInfo)
{
    int                     rowCount = this->ui->packetTable->rowCount();

    this->ui->packetTable->setRowCount(rowCount + 1);
    for (unsigned int col = 0; col < (unsigned int)this->ui->packetTable->columnCount() && col < packetInfo.size(); ++col)
        this->ui->packetTable->setItem(rowCount, col, new QTableWidgetItem(packetInfo[col].c_str()));
}

void                        MainWindow::addPacketLine(std::string &no, std::string &time, std::string &src, std::string &dest,
                                    std::string &prtcl, std::string &len, std::string &info)
{
    int                     rowCount = this->ui->packetTable->rowCount();

    this->ui->packetTable->setRowCount(rowCount + 1);
    this->ui->packetTable->setItem(rowCount, 0, new QTableWidgetItem(no.c_str()));
    this->ui->packetTable->setItem(rowCount, 1, new QTableWidgetItem(time.c_str()));
    this->ui->packetTable->setItem(rowCount, 2, new QTableWidgetItem(src.c_str()));
    this->ui->packetTable->setItem(rowCount, 3, new QTableWidgetItem(dest.c_str()));
    this->ui->packetTable->setItem(rowCount, 4, new QTableWidgetItem(prtcl.c_str()));
    this->ui->packetTable->setItem(rowCount, 5, new QTableWidgetItem(len.c_str()));
    this->ui->packetTable->setItem(rowCount, 6, new QTableWidgetItem(info.c_str()));
}


/*
 * TRANSFORMER TPACKET EN QPACKET
 */

void                        MainWindow::pushPacket(t_packet &packet, std::shared_ptr<Yellow::PCAP::Record> record)
{
    qDebug() << "pushPacket";

    t_QPacket               *packetWidget = new t_QPacket;

    packetWidget->packetTree = this->parsePacket(packet.vecPacket);
    packetWidget->rawDataTable = this->parseData(packet.vecData);
    packetWidget->record = record;
    this->vecPacketWidget.push_back(packetWidget);
}

QTreeWidget                 *MainWindow::parsePacket(std::vector<t_packetTree> vecPacket)
{
    qDebug() << "parsePacket";
    QTreeWidget             *tree = new QTreeWidget(this);

    for (unsigned int i = 0; i < vecPacket.size(); ++i)
    {
        tree->addTopLevelItem(this->setTreeWidgetItem(vecPacket[i]));
    }

    return (tree);
}

QTreeWidgetItem             *MainWindow::setTreeWidgetItem(t_packetTree stringTree)
{
    qDebug() << "setTreeWidgetItem";
    QTreeWidgetItem         *childItem = new QTreeWidgetItem();

    childItem->setText(0, stringTree.main_string.c_str());
    if (!stringTree.vecChildren.empty())
    {
        for (unsigned int i = 0; i < stringTree.vecChildren.size(); ++i)
        {
            childItem->addChild(this->setTreeWidgetItem(stringTree.vecChildren[i]));
        }
    }
    return (childItem);
}

QTableWidget                *MainWindow::parseData(std::vector<t_packetData> vecData)
{
    qDebug() << "parseData";
    QTableWidget            *table = new QTableWidget(vecData.size(), 32, this);

    for (unsigned int iData = 0; iData < vecData.size(); ++iData)
    {
        int                 col = 0;
        for (unsigned int iHex = 0; iHex < ARRAY_SIZE; ++iHex)
        {
            if (!vecData[iData].hexa[iHex].empty())
                table->setItem(iData, col, new QTableWidgetItem(vecData[iData].hexa[iHex].c_str()));
            ++col;
        }
        for (unsigned int iAscii = 0; iAscii < ARRAY_SIZE; ++iAscii)
        {
            if (vecData[iData].ascii[iAscii] != '\0')
                table->setItem(iData, col, new QTableWidgetItem(QChar(vecData[iData].ascii[iAscii])));
            ++col;
        }
    }
    return (table);
}


/*
 *          DISPLAY NEW CONTENTS
 */

void                        MainWindow::fillPacketTree(QTreeWidget *tree)
{
    qDebug() << "start fill packet Tree";
    this->ui->packetTree->clear();

    for (int i = 0; i < tree->topLevelItemCount(); ++i)
        this->ui->packetTree->addTopLevelItem(tree->topLevelItem(i)->clone());

    qDebug() << "end fill packet tree";
}

void                        MainWindow::fillDataTable(QTableWidget *table)
{
    qDebug() << "start fill data table";
    this->ui->dataTable->clear();

    this->ui->dataTable->setRowCount(table->rowCount());
    //this->ui->dataTable->setColumnCount(table->columnCount());

    for (int row = 0; row < table->rowCount(); ++row)
    {
        for (int col = 0; col < table->columnCount(); ++col)
        {
            if (table->item(row, col) != NULL)
            {
                QTableWidgetItem *item = new QTableWidgetItem(table->item(row, col)->text());
                this->ui->dataTable->setItem(row, col, item);
            }
        }
    }
    qDebug() << "end fill data table";
}

void                        MainWindow::clearVecPacketWidget()
{
    qDebug() << "start clearing";
    for (int i = 0; i < this->vecPacketWidget.size(); ++i)
    {
        if (this->vecPacketWidget[i]->packetTree != NULL)
            delete this->vecPacketWidget[i]->packetTree;
        if (this->vecPacketWidget[i]->rawDataTable != NULL)
            delete this->vecPacketWidget[i]->rawDataTable;
        delete this->vecPacketWidget[i];
    }
    this->vecPacketWidget.clear();

    this->ui->packetTable->clear();
    this->ui->packetTable->setRowCount(0);
    this->ui->packetTree->clear();
    this->ui->dataTable->clear();
    this->ui->dataTable->setRowCount(0);
    qDebug() << "end clearing";
}

// SLOTS
void                        MainWindow::on_packetTable_cellClicked(int row, int column)
{
    qDebug() << "Clicked !";
    column = column;

    if (row >= (int)this->vecPacketWidget.size())
        return;

    this->fillPacketTree(this->vecPacketWidget[row]->packetTree);
    this->fillDataTable(this->vecPacketWidget[row]->rawDataTable);

    qDebug() << "Click success!";
}

void                        MainWindow::on_dataTable_cellClicked(int row, int column)
{
    static int      previousRow = -1;
    static int      previousCol = -1;

    if (previousRow != -1 && previousCol != -1)
        this->ui->dataTable->item(previousRow, previousCol)->setBackgroundColor(QColor(255, 255, 255));

    if (column < 16)
    {
        this->ui->dataTable->item(row, column + 16)->setBackgroundColor(QColor(49, 140, 231));
        previousCol = column + 16;
    }
    if (column >= 16)
    {
        this->ui->dataTable->item(row, column - 16)->setBackgroundColor(QColor(49, 140, 231));
        previousCol = column - 16;
    }
    previousRow = row;

}


/*
 *      SAVE & LOAD
 */

void MainWindow::on_actionSave_triggered()
{

    QString             fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("PCAP (*.pcap *.*)"));

    if (fileName.isEmpty())
        return;

    std::ofstream ofs(fileName.toStdString(), std::ofstream::out | std::ofstream::trunc | std::ofstream::binary);
    if (!ofs)
        std::cout << "Fail to open file. Exiting Yellow." << std::endl;
    else
    {
        ofs << this->rec;
        ofs.close();
        this->ui->labelGlobalInfo->setText("File successfully saved");
    }
}

void MainWindow::on_actionLoad_triggered()
{
    QString             fileName;
    QFileDialog         dialog(this);

    dialog.setFileMode(QFileDialog::ExistingFile);
    fileName = dialog.getOpenFileName(this, tr("Open File"), "", tr("PCAP (*.pcap);;All files (*.*)"));

    if (fileName.isEmpty())
        return ;

    if (this->ui->actionStop->isEnabled())
        this->on_actionStop_triggered();                         // Stop sniffing

    std::ifstream ifs(fileName.toStdString(), std::ifstream::binary);
    if (!ifs)
        std::cout << "Fail to open file. Exiting Yellow." << std::endl;
    else
    {
        Yellow::PCAP::Recorder  newRec;
        this->rec = newRec;                                             // Reinitialize
        ifs >> newRec;                                            // Fill new rec

        for (unsigned int i = 0; i < newRec.getRecords().size(); ++i)   // Recreate everything
            this->receiveNewPacket(newRec.getRecords()[i]->getPacket());
        ifs.close();
        this->ui->labelGlobalInfo->setText("File successfully loaded");
    }
}




/*
 *          START & STOP
 */

void MainWindow::on_actionStart_triggered()
{
    snifThread->start();

    this->ui->actionStart->setDisabled(true);
    this->ui->actionStop->setEnabled(true);
    this->ui->labelGlobalInfo->setText("Sniffing...");
}

void MainWindow::on_actionStop_triggered()
{
    this->ui->actionStart->setEnabled(true);
    this->ui->actionStop->setDisabled(true);

    this->snif.stop();
    this->ui->labelGlobalInfo->setText("Stopping sniffing...");
    this->ppjWait(1); // UI QT
    this->snifThread->wait();
    if (this->snifThread->isFinished())
        qDebug() << "Sniffing Thread finished";
    this->ui->labelGlobalInfo->setText("Stopped");
}




/*
 *      FILTERS
 */

void MainWindow::on_actionFilter_triggered()
{
    static bool show = true;

    this->ui->filteredTable->clear();
    this->ui->filteredTable->setRowCount(0);
    this->ui->filteredTable->hide();
    this->ui->packetTable->show();
    if (show)
        this->ui->groupBox->show();
    else
        this->ui->groupBox->hide();
    show = !show;
}

void MainWindow::on_btnApply_clicked()
{
    QString         no;
    QString         time1;
    QString         time2;
    QString         length;
    QString         source;
    QString         dest;
    QString         proto;
    std::vector<QString>    vecFilters;

    this->ui->filteredTable->clear();
    this->ui->filteredTable->setRowCount(0);

    if (this->ui->checkNo->isChecked())
        no = QString::number(this->ui->noSpinBox->value());
    if (this->ui->checkTime->isChecked())
    {
        time1 = this->ui->dateTimeEdit->dateTime().toString("yyyy-MM-dd HH:mm:ss.zzz");
        time2 = this->ui->dateTimeEdit_2->dateTime().toString("yyyy-MM-dd HH:mm:ss.zzz");
    }
    if (this->ui->checkLength->isChecked())
        length = QString::number(this->ui->lengthSpinBox->value());
    if (this->ui->checkSource->isChecked())
        source = this->ui->sourceLineEdit->text();
    if (this->ui->checkDest->isChecked())
        dest = this->ui->destinationLineEdit->text();
    if (this->ui->checkProto->isChecked())
        proto = this->ui->protocolComboBox->currentText();

    vecFilters.push_back(no);
    vecFilters.push_back(time1);
    vecFilters.push_back(time2);
    vecFilters.push_back(length);
    vecFilters.push_back(source);
    vecFilters.push_back(dest);
    vecFilters.push_back(proto);
    this->filterPacket(vecFilters);
}

void MainWindow::on_btnCancel_clicked()
{
    this->on_actionFilter_triggered();
}

void    MainWindow::filterPacket(std::vector<QString> vecFilters)
{
    std::vector<int>        vecIndex;                   // Stores index row of all packet matching the filters
    bool                    firstResearch = true;      // Used to know if we need to look in packetTable or use vecIndex to access already compatible items
    int                     column;

    for (int iFilters = 0; iFilters < vecFilters.size(); ++iFilters)
    {
        if (iFilters > 2)
            column = iFilters - 1;
        else
            column = iFilters;


        if (iFilters == 2 || vecFilters[iFilters].isEmpty()) // 2 = time2, so time2 need to be ignored as it is related to time1
            continue;

        if (iFilters == 0 || iFilters > 2)
        {
            if (firstResearch)
            {
                for (int row = 0; row < this->ui->packetTable->rowCount(); ++row)
                {
                    if (vecFilters[iFilters].compare(this->ui->packetTable->item(row, column)->text()) == 0)
                        vecIndex.push_back(row);
                }
                firstResearch = false;
            }
            else
            {
                int iVec = 0;
                while (iVec < vecIndex.size())
                {
                    if (vecFilters[iFilters].compare(this->ui->packetTable->item(vecIndex[iVec], column)->text()) != 0) // if != 0, pop it, otherwise, keep it because it still matches filters
                        vecIndex.erase(vecIndex.begin() + iVec);
                    else
                        ++iVec;
                }
            }

        }
        else if (iFilters == 1)
        {
            QDateTime   time1 = QDateTime::fromString(vecFilters[iFilters], "yyyy-MM-dd HH:mm:ss.zzz");
            QDateTime   time2 = QDateTime::fromString(vecFilters[iFilters + 1], "yyyy-MM-dd HH:mm:ss.zzz");

            if (firstResearch)
            {
                for (int row = 0; row < this->ui->packetTable->rowCount(); ++row)
                {
                    QDateTime time = QDateTime::fromString(this->ui->packetTable->item(row, column)->text(), "yyyy-MM-dd HH:mm:ss.zzz");
                    if (time >= time1 && time <= time2)
                        vecIndex.push_back(row);
                }
                firstResearch = false;
            }
            else
            {
                int iVec = 0;
                while (iVec < vecIndex.size())
                {
                    QDateTime time = QDateTime::fromString(this->ui->packetTable->item(vecIndex[iVec], column)->text(), "yyyy-MM-dd HH:mm:ss.zzz");
                    if (time >= time1 && time <= time2) // if < or >, pop it, otherwise, keep it because it still matches filters
                        ++iVec;
                    else
                        vecIndex.erase(vecIndex.begin() + iVec);
                }
            }
        }
    }
    if (vecIndex.empty())
    {
        this->ui->labelGlobalInfo->setText("No matches found. Try changing your filters.");
        this->ui->filteredTable->setRowCount(0);
    }
    else
    {
        this->ui->labelGlobalInfo->setText(QString::number(vecIndex.size()) + QString(" packet match the filters."));
        this->fillFilteredTable(vecIndex);
    }
}

void            MainWindow::fillFilteredTable(std::vector<int> vecIndex)
{
    // Copy lines from packetTable using rowIndexes in vecIndex
    for (int iVec = 0; iVec < vecIndex.size(); ++iVec)
    {
        this->ui->filteredTable->setRowCount(this->ui->filteredTable->rowCount() + 1);
        for (int col = 0; col < this->ui->filteredTable->columnCount(); ++col)
        {
            this->ui->filteredTable->setItem(this->ui->filteredTable->rowCount() - 1, col,
                                             new QTableWidgetItem(this->ui->packetTable->item(vecIndex[iVec], col)->text()));
        }
    }

    this->ui->packetTable->hide();
    this->ui->filteredTable->show();
}


/*
 *  FILTERS CHECKBOXES
 */

void MainWindow::on_checkNo_stateChanged(int arg1)
{
    if (arg1 == Qt::Unchecked)
        this->ui->noSpinBox->setDisabled(true);
    else if (arg1 == Qt::Checked)
        this->ui->noSpinBox->setEnabled(true);
}

void MainWindow::on_checkTime_stateChanged(int arg1)
{
    if (arg1 == Qt::Unchecked)
    {
        this->ui->dateTimeEdit->setDisabled(true);
        this->ui->dateTimeEdit_2->setDisabled(true);
    }
    else if (arg1 == Qt::Checked)
    {
        this->ui->dateTimeEdit->setEnabled(true);
        this->ui->dateTimeEdit_2->setEnabled(true);
    }
}

void MainWindow::on_checkLength_stateChanged(int arg1)
{
    if (arg1 == Qt::Unchecked)
        this->ui->lengthSpinBox->setDisabled(true);
    else if (arg1 == Qt::Checked)
        this->ui->lengthSpinBox->setEnabled(true);
}


void MainWindow::on_checkSource_stateChanged(int arg1)
{
    if (arg1 == Qt::Unchecked)
        this->ui->sourceLineEdit->setDisabled(true);
    else if (arg1 == Qt::Checked)
        this->ui->sourceLineEdit->setEnabled(true);
}


void MainWindow::on_checkDest_stateChanged(int arg1)
{
    if (arg1 == Qt::Unchecked)
        this->ui->destinationLineEdit->setDisabled(true);
    else if (arg1 == Qt::Checked)
        this->ui->destinationLineEdit->setEnabled(true);
}


void MainWindow::on_checkProto_stateChanged(int arg1)
{
    if (arg1 == Qt::Unchecked)
        this->ui->protocolComboBox->setDisabled(true);
    else if (arg1 == Qt::Checked)
        this->ui->protocolComboBox->setEnabled(true);
}


void MainWindow::on_filteredTable_cellClicked(int row, int column)
{
    // Send No contained in first column matching on the right row in packetTable, then emit the signal to display the informations;
    int         packetTableRow = this->ui->filteredTable->item(row, 0)->text().toInt();

    this->on_packetTable_cellClicked(packetTableRow, column);
}


void MainWindow::ppjWait (int ms)
{
    QEventLoop eventLoop;
    QTimer::singleShot (ms, &eventLoop, SLOT (quit ()));
    eventLoop.exec ();
}
